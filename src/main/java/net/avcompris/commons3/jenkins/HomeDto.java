package net.avcompris.commons3.jenkins;

import net.avcompris.binding.annotation.XPath;

@XPath("/*")
public interface HomeDto extends JobDtoContainer {

	@XPath("job")
	@Override
	Job[] getJobs();
}
