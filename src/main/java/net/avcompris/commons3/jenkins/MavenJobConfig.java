package net.avcompris.commons3.jenkins;

import net.avcompris.binding.annotation.XPath;

@XPath("/maven2-moduleset")
public interface MavenJobConfig extends JobConfig {

	@XPath("rootModule")
	Module getRootModule();

	@XPath("goals")
	String getGoals();

	@XPath("settings[@class = 'jenkins.mvn.FilePathSettingsProvider']")
	Settings getSettings();

	interface Module {

		@XPath("groupId")
		String getGroupId();

		@XPath("artifactId")
		String getArtifactId();
	}

	interface Settings {

		@XPath("path")
		String getPath();
	}
}
