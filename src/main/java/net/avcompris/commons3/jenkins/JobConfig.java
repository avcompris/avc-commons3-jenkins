package net.avcompris.commons3.jenkins;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;

@XPath("/*")
public interface JobConfig {

	@XPath("scm")
	SCM getSCM();

	@XPath("assignedNode")
	String getAssignedNode();

	@XPath("not(canRoam = 'false')")
	boolean getCanRoam();

	@XPath("not(disabled = 'false')")
	boolean isDisabled();

	@XPath("not(blockBuildWhenDownstreamBuilding = 'false')")
	boolean getBlockBuildWhenDownstreamBuilding();

	@XPath("not(blockBuildWhenUpstreamBuilding = 'false')")
	boolean getBlockBuildWhenUpstreamBuilding();

	@XPath("triggers/*")
	Trigger[] getTriggers();

	interface SCM {

		@XPath("name()")
		String getType();

		@XPath(value = "self::scm", function = "@class = 'hudson.scm.NullSCM'")
		boolean isNullSCM();

		@XPath("@class = 'hudson.plugins.git.GitSCM'")
		boolean isGitSCM();
	}

	interface GitSCM extends SCM {

		@XPath("@class")
		String getClassName();

		@XPath("@plugin")
		String getPlugin();

		@XPath("userRemoteConfigs/hudson.plugins.git.UserRemoteConfig")
		UserRemoteConfig[] getUserRemoteConfigs();
	}

	interface UserRemoteConfig {

		@XPath("url")
		String getUrl();

		@XPath("credentialsId")
		@Nullable
		String getCredentialsId();
	}

	interface Trigger {

		@XPath("name()")
		String getType();

		@XPath("self::hudson.triggers.SCMTrigger")
		boolean isSCMTrigger();
	}

	interface SCMTrigger extends Trigger {

		@XPath("spec")
		String getSpec();

		@XPath("not(ignorePostCommitHooks = 'false')")
		boolean getIgnorePostCommitHooks();
	}
}
