package net.avcompris.commons3.jenkins;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;

public interface JobDtoContainer {

	@XPath("job")
	Job[] getJobs();

	enum JobClass {

		MavenModuleSet("hudson.maven.MavenModuleSet"), //
		Folder("com.cloudbees.hudson.plugins.folder.Folder"), //
		FreeStyleProject("hudson.model.FreeStyleProject");

		private JobClass(final String className) {

			this.className = checkNotNull(className, "className");
		}

		private final String className;

		@Override
		public String toString() {

			return className;
		}
	}

	enum Color {

		BLUE("blue"), BLUE_ANIME("blue_anime");

		private Color(final String label) {

			this.label = checkNotNull(label, "label");
		}

		private final String label;

		@Override
		public String toString() {

			return label;
		}
	}

	interface Job {

		@XPath("@_class")
		JobClass getJobClass();

		@XPath("name")
		String getName();

		@XPath(value = "url", function = "normalize-space()")
		String getURL();

		@Nullable
		@XPath("color")
		Color getColor();
	}
}
