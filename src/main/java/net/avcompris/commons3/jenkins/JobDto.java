package net.avcompris.commons3.jenkins;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;

@XPath("/*")
public interface JobDto extends JobDtoContainer {

	@XPath("name")
	String getName();

	@XPath("@_class")
	JobClass getJobClass();

	@XPath("url")
	String getURL();

	@XPath("build")
	@Nullable
	Build[] getBuilds();

	@XPath("job")
	@Nullable
	@Override
	HomeDto.Job[] getJobs();

	@Nullable
	@XPath("build[number = $arg0]")
	Build getBuildByNumber(int buildNumber);

	@Nullable
	@XPath("firstBuild")
	Build getFistBuild();

	@Nullable
	@XPath("lastBuild")
	Build getLastBuild();

	@Nullable
	@XPath("lastCompletedBuild")
	Build getLastCompletedBuild();

	@Nullable
	@XPath("lastFailedBuild")
	Build getLastFailedBuild();

	@Nullable
	@XPath("lastStableBuild")
	Build getLastStableBuild();

	@Nullable
	@XPath("lastSuccessfulBuild")
	Build getLastSuccessfulBuild();

	@Nullable
	@XPath("lastUnstableBuild")
	Build getLastUnstableBuild();

	@Nullable
	@XPath("lastUnsuccessfulBuild")
	Build getLastUnsuccessfulBuild();

	@XPath("upstreamProject")
	HomeDto.Job[] getUpstreamProjects();

	interface Build {

		@XPath("number")
		int getBuildNumber();

		@XPath(value = "url", function = "normalize-space()")
		String getURL();
	}
}
