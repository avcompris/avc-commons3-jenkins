package net.avcompris.commons3.jenkins;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;

@XPath("/*")
public interface BuildDto {

	@XPath("id")
	int getBuildNumber();

	@XPath("url")
	String getURL();

	@XPath("building")
	boolean isBuilding();

	@XPath("inQueue")
	boolean isInQueue();

	@Nullable
	@XPath("result")
	Result getResult();

	enum Result {

		SUCCESS, FAILURE, UNSTABLE, ABORTED,
	}

	@Nullable
	@XPath("action[@_class = 'hudson.plugins.git.util.BuildData']/remoteUrl")
	String getGitRemoteUrl();

	@Nullable
	@XPath("action[@_class = 'hudson.plugins.git.util.BuildData']/lastBuiltRevision/SHA1")
	String getLastBuiltRevision();

	@XPath("timestamp")
	long getTimestamp();

	@XPath("duration")
	int getDuration();

	@XPath("builtOn")
	String getBuiltOn();

	@XPath("action[@_class = 'hudson.maven.reporters.SurefireAggregatedReport']")
	TestReport getTestReport();

	interface TestReport {

		@XPath("failCount")
		int getFailCount();

		@XPath("skipCount")
		int getSkipCount();

		@XPath("totalCount")
		int getTotalCount();
	}

	@XPath("artifact")
	int sizeOfArtifacts();

	Artifact[] getArtifacts();

	interface Artifact {

		@XPath("fileName")
		String getFileName();

		@XPath("relativePath")
		String getRelativePath();
	}

	@XPath("action[@_class = 'hudson.model.CauseAction']/cause/@_class = 'hudson.model.Cause$UpstreamCause'")
	boolean isCauseUpstream();

	@XPath("action[@_class = 'hudson.model.CauseAction']/cause[@_class = 'hudson.model.Cause$UpstreamCause']"
			+ "/upstreamProject")
	String getUpstreamProject();

	@XPath("action[@_class = 'hudson.model.CauseAction']/cause[@_class = 'hudson.model.Cause$UpstreamCause']"
			+ "/upstreamBuild")
	int getUpstreamBuild();
}
