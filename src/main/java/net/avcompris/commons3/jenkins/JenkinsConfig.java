package net.avcompris.commons3.jenkins;

import net.avcompris.binding.annotation.XPath;

@XPath("/hudson")
public interface JenkinsConfig {

	@XPath("clouds/com.nirima.jenkins.plugins.docker.DockerCloud")
	DockerCloud[] getDockerClouds();

	interface DockerCloud {

		@XPath("name")
		String getName();

		@XPath("disabled/disabledByChoice = 'false'")
		boolean isEnabled();

		@XPath("dockerApi")
		DockerApi getDockerApi();

		@XPath("templates/com.nirima.jenkins.plugins.docker.DockerTemplate")
		DockerAgentTemplate[] getDockerAgentTemplates();
	}

	interface DockerApi {

		@XPath("dockerHost")
		DockerHost getDockerHost();
	}

	interface DockerHost {

		@XPath("uri")
		String getUri();
	}

	interface DockerAgentTemplate {

		@XPath("labelString")
		String getLabelString();

		@XPath("remoteFs")
		String getRemoteFs();

		@XPath("instanceCap")
		int getInstanceCap();

		@XPath("dockerTemplateBase")
		DockerTemplateBase getDockerTemplateBase();

		@XPath("name")
		String getName();

		@XPath("pullStrategy")
		String getPullStrategy();

		@XPath("removeVolumes = 'true'")
		boolean getRemoveVolumes();
	}

	interface DockerTemplateBase {

		@XPath("image")
		String getImage();

		@XPath("dockerCommand")
		String getDockerCommand();

		@XPath("environment/string")
		String[] getEnvironment();

		@XPath("bindPorts")
		String getBindPorts();
	}

	@XPath("clouds/com.github.kostyasha.yad.DockerCloud")
	YadCloud[] getYadClouds();

	interface YadCloud {

		@XPath("name")
		String getName();

		@XPath("connector")
		YadConnector getConnector();

		@XPath("templates/com.github.kostyasha.yad.DockerSlaveTemplate[not(@reference)]" //
				+ " | provisionedImages/entry/com.github.kostyasha.yad.DockerSlaveTemplate")
		YadTemplate[] getTemplates();
	}

	interface YadConnector {

		@XPath("serverUrl")
		String getServerUrl();
	}

	interface YadTemplate {

		@XPath("id")
		String getId();

		@XPath("labelString")
		String getLabelString();

		@XPath("remoteFs")
		String getRemoteFs();

		@XPath("dockerContainerLifecycle")
		YadContainerLifecycle getDockerContainerLifecycle();
	}

	interface YadContainerLifecycle {

		@XPath("image")
		String getImage();

		@XPath("bindPorts")
		String getBindPorts();
	}
}
